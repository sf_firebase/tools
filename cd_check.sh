#!/bin/bash

CUSTOM_DOMAIN=$1
A=$(dig +short $CUSTOM_DOMAIN a)
TXT=$(dig +short $CUSTOM_DOMAIN txt)
CAA=$(dig +short $CUSTOM_DOMAIN caa)
AAAA=$(dig +short $CUSTOM_DOMAIN aaaa) 
CNAME=$(dig +short $CUSTOM_DOMAIN cname)
if [[ $(openssl s_client -servername $CUSTOM_DOMAIN -connect $CUSTOM_DOMAIN:443 </dev/null 2>/dev/null | openssl x509 -noout -text | grep DNS:$CUSTOM_DOMAIN) ]]; then SAN=$CUSTOM_DOMAIN; else SAN=NOK; fi
DATES=$(openssl s_client -connect $CUSTOM_DOMAIN:443 </dev/null 2>/dev/null | openssl x509 -noout -startdate -enddate)

echo ""
echo "Testing $CUSTOM_DOMAIN custom domin with use of:"
echo "https://gitlab.com/sf_firebase/tools/-/blob/main/cd_check.sh"
echo ""
echo "SAN:   $SAN"
echo "A:     $A"
echo "TXT:   $TXT"
echo "CAA:   $CAA"
echo "AAAA:  $AAAA"
echo "CNAME: $CNAME"
echo "$DATES"
echo ""
